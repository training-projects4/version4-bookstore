import React from 'react';

export default function Title ({prodName,label}){
    return(
    
        <div>
        <h1 className="text-capitalize font-weight-bold">{prodName}<strong className="text-yellow">{label}</strong>
        </h1>
        </div>
    )
}