import React, { useState, useEffect, useContext } from 'react';
import { NavLink } from 'react-router-dom';
import { Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

 
export default function Registration (){
     const navigate = useNavigate();
    const { user, setUser } = useContext(UserContext);
    const [ name, setName ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ verifyPassword, setVerifyPassword ] = useState('');
	const [ isActive, setIsActive ] = useState(true);
    
    useEffect(() => {
		if((name !== '' && email !== '' && password !== '' && verifyPassword !== '') && (password === verifyPassword)){
			setIsActive(true);
		}else {
			setIsActive(false);
		}
	}, [name, email, password, verifyPassword])

    function handleFormSubmit(e){
        e.preventDefault();
    
    fetch('https://dandbookstore.herokuapp.com/api/auth/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				name: name,
                email: email,
				password: password
			})
		})
		.then(response => response.json())
		.then(data => {
			if(data!== undefined){
				localStorage.setItem('data', data);
				setUser({
					data: data
				})
				navigate ('/')
			
				Swal.fire({
					title: 'Yaay!',
					icon: 'success',
					text: 'You are now registered. Enjoy shopping!'
				})
			}
				else{
					Swal.fire({
						title: 'Ooopsss',
						icon: 'error',
						text: 'Something went wrong. Check your credentials.'
					})
				}
			
				setEmail('');
				setPassword('');
				setVerifyPassword ('');
				
})
}

    return(
        (user.accessToken !== null) ? 

		<Navigate to="/" />
		:
   
        <div className='main-wrapper'>
            <div className='app-wrapper'>
                <div>
                    <h2 className='title'>Create Account</h2>
                </div>
                <form className='form-wrapper' onSubmit={e => handleFormSubmit (e)}>
                <div className='name'>
                        <label className='label'>Name</label>
                        <input className='input' type="text" name="name" value={name} onChange={e=>setName(e.target.value)}required />
                    </div>
                    <div className='email'>
                        <label className='label'>Email</label>
                        <input className='input' type="email" name="email" value={email} onChange={e=>setEmail(e.target.value)}required />
                    </div>
                    <div className='password'>
                        <label className='label'>Password </label>
                        <input className='input' type="password" name="password" value={password} onChange={e=>setPassword(e.target.value)} required />
                    </div>
                    <div className='password'>
                        <label className='label'>Verify Password </label>
                        <input className='input' type="password" name="password" value={verifyPassword} onChange={e=>setVerifyPassword(e.target.value)} required />
                    </div>
                    
                    
                        {isActive ?
                        <button className='submit'>Register</button>
                        :
                        <button className='submit' disabled>Register</button>
                        }
                    

                    
                    <small className='clickHere'>Already have an account?
                    <NavLink to="/login" style={{textDecoration:'none'}}><strong> Click here </strong> </NavLink>to login.</small>
                
                  

                </form>

            </div>
        </div>
    )
}
