// import Link from '@material-ui/core/Link';
import { useContext} from 'react';
import { Navbar, Nav } from 'react-bootstrap';
// import { Link } from 'react-router-dom';
import UserContext from '../../UserContext';

import {
  Box,
  Divider,
  List,
  Link,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Stack,
  Typography,
} from "@mui/material";
import {
  AppbarActionIcons,
  AppbarContainer,
  AppbarHeader,
  MyList,
} from "../../styles/appbar";
import PersonIcon from "@mui/icons-material/Person";
import FavoriteIcon from "@mui/icons-material/Favorite";
import SearchIcon from "@mui/icons-material/Search";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Actions from "./actions";
import { useUIContext } from "../../context/ui";

export default function AppbarDesktop({ matches }) {
  const { user } = useContext(UserContext);
  const { setShowSearchBox } = useUIContext();

  return (
    <AppbarContainer>
      <AppbarHeader variant="h4">D &#38; D</AppbarHeader>
      <MyList type="row">
       
      <Link style={{textDecoration: 'none'}} href="/">
        <ListItemText primary="Home" className="px-5"/>
        </Link>
        <Link style={{textDecoration: 'none'}} href="#">
        <ListItemText primary="Categories" className="px-5"/>
        </Link>
        <Link style={{textDecoration: 'none'}} href="/about">
        <ListItemText primary="About us" className="px-5"/>
        </Link>
        <Link style={{textDecoration: 'none'}} href="/contact">
        <ListItemText primary="Contact us" className="px-5"/>
        </Link>
      
        <ListItemButton onClick={() => setShowSearchBox(true)}>
          <ListItemIcon>
            <SearchIcon />
          </ListItemIcon>
        </ListItemButton>
          </MyList>
       <Actions matches={matches} />   
    </AppbarContainer>
  );
}